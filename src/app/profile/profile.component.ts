import { GithubService } from './../github.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user[];
  repos[];
  username: string;

  constructor(private _githubService: GithubService) {
    this.user = false;
   }

  ngOnInit() {
  }

  searchUser() {
    this._githubService.updateUser(this.username);

    this._githubService.getUser().subscribe(user => {
      // console.log(user);
      this.user = user;
    });

    this._githubService.getRepos().subscribe(repos => {
      // console.log(user);
      this.repos = repos;
    });
  }

}
