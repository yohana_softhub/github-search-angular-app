import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GithubService {
  private username: string;
  private client_id = '659d3910a06172603027';
  private client_secret = 'dc1b6f861b9d0c56507fd8a41239b86a44af1276';

  constructor(private _http: HttpClient) {
    // console.log('Github service is working...');
    this.username = 'owncloud';
  }

getUser() {
  // tslint:disable-next-line:max-line-length
  return this._http.get('http://api.github.com/users/' + this.username + '?client_id=' + this.client_id + '&client_secret=' + this.client_secret);
}

getRepos() {
  // tslint:disable-next-line:max-line-length
  return this._http.get('http://api.github.com/users/' + this.username + '/repos' + '?client_id=' + this.client_id + '&client_secret=' + this.client_secret);
}

updateUser(username) {
  this.username = username;
}

}
